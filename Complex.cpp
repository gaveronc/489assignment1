#include "Complex.h"

void Complex::SetReal(double x) {
	real = x;
}

void Complex::SetImaginary(double x) {
	imaginary = x;
}

double Complex::GetReal() {
	return real;
}

double Complex::GetImaginary() {
	return imaginary;
}

//Magnitude is the square root of the sum of the squares
double Complex::GetMagnitude() {
	return sqrt((real*real)+(imaginary*imaginary));
}

//Angle is tan^-1(imaginary/real)
double Complex::GetAngle() {
	return atan2(imaginary,real);
}