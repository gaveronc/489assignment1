#ifndef COMPLEX__H
#define COMPLEX__H

#include <cmath> //Used for atan2 function

class Complex {
	public:
		void SetReal(double x);
		void SetImaginary(double x);
		
		//Rectangular values
		double GetReal();
		double GetImaginary();
		
		//Polar values, calculated as needed
		double GetMagnitude();
		double GetAngle();
	private: //Only stores rectangular coordinates
		double real;
		double imaginary;
};

#endif