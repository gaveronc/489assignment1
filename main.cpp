/*
* Cameron Gaveronski
* 200230075
* ENEL 487
* Assignment 1
* Due September 22, 2016
*/

#include <iostream>  //Used for I/O
#include <sstream>   //Used to convert strings to doubles
#include <cmath>     //Used for sin/cos functions
#include "Complex.h" //Complex data type definition

using namespace std;

//Converts a string to a double
double StringToInt (string input) {
	stringstream stream;
	double strDouble;
	
	//Ensure the stream is empty
	stream.clear();
	//Put the string into the stream
	stream << input;
	//Get a double out of the stream
	stream >> strDouble;
	
	return strDouble;
}

//Adds complex number b to a and stores the result in the last argument
void Add(Complex a, Complex b, Complex *result) {
	//Add b components to a components
	result->SetReal(a.GetReal() + b.GetReal());
	result->SetImaginary(a.GetImaginary() + b.GetImaginary());
}

//Subtracts complex number b from a and stores the result in the last argument
void Subtract(Complex a, Complex b, Complex *result) {
	//Subtracy b components from a components
	result->SetReal(a.GetReal() - b.GetReal());
	result->SetImaginary(a.GetImaginary() - b.GetImaginary());
}

//Multiplies complex number b and a and stores the result in the last argument
void Multiply(Complex a, Complex b, Complex *result) {
	double magnitude;
	double angle;
	
	//Multiply magnitudes
	magnitude = a.GetMagnitude()*b.GetMagnitude();
	//Add angles
	angle = a.GetAngle()+b.GetAngle();
	//Convert to rectangular form
	result->SetReal(cos(angle)*magnitude);
	result->SetImaginary(sin(angle)*magnitude);
}

//Divides complex number a by b and stores the result in the last argument
void Divide(Complex a, Complex b, Complex *result) {
	double magnitude;
	double angle;
	
	//Divide magnitudes
	magnitude = a.GetMagnitude()/b.GetMagnitude();
	//Subtract angles
	angle = a.GetAngle()-b.GetAngle();
	//Convert to rectangular form
	result->SetReal(cos(angle)*magnitude);
	result->SetImaginary(sin(angle)*magnitude);
}

//This program assumes the input is correct, meaning the following order:
//<operation> <real component 1> <imaginary component 1> <real component 2> <imaginary component 2>
//This convention is followed in both interactive mode and batch mode
int main(int argc, char *argv[]) {
	Complex a, b, result;
	
	//This determines which of the above functions are called, as well as exiting on a 'q'
	char operation = ' ';
	
	if (argc == 1) {
		//Interactive mode
	} else if (argc == 6) {
		//Batch mode, used as a function call
		//Follows convention specified above
		operation = argv[1][0];
		a.SetReal(StringToInt(argv[2]));
		a.SetImaginary(StringToInt(argv[3]));
		b.SetReal(StringToInt(argv[4]));
		b.SetImaginary(StringToInt(argv[5]));
		
		//Choose which operation to perform on the data
		switch (operation) {
			case 'A':
			case 'a':
				Add(a, b, &result);
				break;
			case 'S':
			case 's':
				Subtract(a, b, &result);
				break;
			case 'M':
			case 'm':
				Multiply(a, b, &result);
				break;
			case 'D':
			case 'd':
				Divide(a, b, &result);
				break;
			default:
				cout << "Bad operation\n";
		}
		
		//Format output
		cout << result.GetReal();
		if (result.GetImaginary() < 0) {
			cout << " - j " << -result.GetImaginary();
		} else {
			cout << " + j " << result.GetImaginary();
		}
		cout << endl;
		operation = 'q'; //Exits the program
	} else {
		cout << "Bad arguments\n";
	}
	
	//Interactive mode loop
	//Exits if operation is 'q'
	while (operation != 'q' && operation != 'Q') {
		double temp = 0;
		cin >> operation;
		if (operation != 'q' && operation != 'Q') {
			cin >> temp;
			a.SetReal(temp);
			cin >> temp;
			a.SetImaginary(temp);
			cin >> temp;
			b.SetReal(temp);
			cin >> temp;
			b.SetImaginary(temp);
			
			//Choose which operation to perform on the data
			switch (operation) {
				case 'A':
				case 'a':
					Add(a, b, &result);
					break;
				case 'S':
				case 's':
					Subtract(a, b, &result);
					break;
				case 'M':
				case 'm':
					Multiply(a, b, &result);
					break;
				case 'D':
				case 'd':
					Divide(a, b, &result);
					break;
				default:
					cout << "Bad operation\n";
			}
			
			//Format output
			cout << result.GetReal();
			if (result.GetImaginary() < 0) {
				cout << " - j " << -result.GetImaginary();
			} else {
				cout << " + j " << result.GetImaginary();
			}
			cout << endl;
		}
	}
	
	return 0;
}