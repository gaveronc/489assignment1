This program performs mathematical operations on complex numbers, taking input
in the form "operation real_1 imaginary_1 real_2 imaginary_2", where
"operation" is one of the characters "a" "s" "m" "d" or "q", which correspond to
addition, subtraction, multiplication, division, and stopping the program,
respectively. These are not case-sensitive.

Note that the program generally does -not- handle erroneous input; to use this
program, the user must enter data that can be interpreted as a
double-precision floating point number, otherwise the program may not behave
as intended. Erroneous operations, however, will be ignored with an error
message displayed to the user.

The program has two modes of operation, which depend on the number of
arguments used to invoke the executable. If no arguments are present, then the
program is launched in interactive mode where the user will be continually
prompted for operations and data until the operation "q" is provided.

The program can also be executed with five arguments which will perform a
single operation and then exit, using the same argument order as the
interactive mode.

The program can be compiled using the following command:
g++ main.cpp Complex.cpp -o Complex

A set of test data can be piped to the executable from the provided FileIO.txt
by invoking the executable as follows:
./Complex < FileIO.txt
